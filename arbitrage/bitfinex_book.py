import time
from queue import Empty
from collections import OrderedDict

from threading import Thread, Event

from django.db import OperationalError

from arbitrage.ibook import IBookHandler
from arbitrage.ibook import BookData

from .models import Exchange


class BitfinexBook(Thread, IBookHandler):
    """
    Bitfinex order book implementation.
    """
    def __init__(self, socket, book, *args, **kwargs):
        """
        Creates BitfinexBook with order book (subscription) name and optional
        web socket reference. To start web socket message processing use start()
        interface. Highest 3 bids and asks are stored into database.

        :param socket: Bitfinex Web socket API reference.
        :param book: Book (subscription) name.
        :param args: Optional args passed to Thread.
        :param kwargs: Optional kwargs passed to Thread.
        """
        IBookHandler.__init__(self, socket, __name__, book, *args, **kwargs)
        Thread.__init__(self, *args, **kwargs)

        self._exchange_db = None
        self._book_db = None

        # database init
        if not Exchange.objects.filter(name='Bitfinex').exists():
            self._exchange_db = Exchange(name='Bitfinex')
            self._exchange_db.save()
        else:
            self._exchange_db = Exchange.objects.filter(name='Bitfinex').first()

        self._stopped = Event()
        self._init = True

    def stop(self, timeout=None):
        """Set sentinel for run() method and join thread.
        :param timeout: Thread.join timeout
        """
        self._log.info("Unsubscribing '%s'", self.symbol)

        self._wss.unsubscribe_from_order_book(self.symbol)

        self._stopped.set()
        super(BitfinexBook, self).join(timeout=timeout)

    def run(self):
        """
        Main routine. Processing web socket order book data. Additionally
        establishes web socket connection with Bitfinex and subscribes to the
        configured order book(s).
        """
        if not self._wss.conn.connected.is_set():
            self._wss.start()

            while not self._wss.conn.connected.is_set():
                time.sleep(0.25)

            self._wss.subscribe_to_order_book(self.symbol)

        # delay processing of data
        booker_q = None
        while booker_q is None:
            try:
                booker_q = self._wss.books(self.symbol)
            except KeyError:
                time.sleep(0.25)

        self._book_db = self._exchange_db.subscribe_book(self.symbol)

        while not self._stopped.is_set():
            try:
                while not booker_q.empty():
                    data = booker_q.get()
                    self._handle_data(data)
            except Empty:
                continue

    def symbol_mapping(self):
        """ :returns: Empty string. """
        return ""

    def _handle_data(self, data):
        """
        Handles book data. Highest 3 bids and asks are stored in the database.
        Acknowledgment:
        https://gist.github.com/prdn/b8c067c758aab7fa3bf715101086b47c
        :param data: Web socket order book data.
        """
        data, timestamp = data
        data = data[0]

        # handle book snapshot and create in-memory book structure
        if self._init:
            for fields in data:
                pp = BookData(float(fields[0]), fields[1], fields[2], timestamp)
                target = self._book.bids if pp.amount >= 0 else self._book.asks
                pp.amount = abs(pp.amount)
                target.update({pp.price: pp})

            self._init = False
        # handle book updates
        else:
            pp = BookData(float(data[0]), data[1], data[2], timestamp)
            if 0 == pp.count:
                if pp.amount > 0:
                    try:
                        del self._book.bids[pp.price]
                    except KeyError:
                        self._log.debug("Unable to delete book, bid not found")
                elif pp.amount < 0:
                    try:
                        del self._book.asks[pp.price]
                    except KeyError:
                        self._log.debug("Unable to delete book, ask not found")
            else:
                target = self._book.bids if pp.amount > 0 else self._book.asks
                pp.amount = abs(pp.amount)
                target.update({pp.price: pp})

        self._store_book()

    def _store_book(self):
        """ Stores currently highest 3 bids and asks to the database. """
        # order books
        obids = OrderedDict(sorted(self._book.bids.items()))
        oasks = OrderedDict(sorted(self._book.asks.items()))

        try:
            # store highest 3 bids and asks
            self._book_db.update_bids(list(obids.values())[-3:])
            self._book_db.update_asks(list(oasks.values())[-3:])
        except OperationalError:
            # this error only indicates that sqlite is experiencing more
            # concurrency than sqlite can handle in default configuration
            pass
