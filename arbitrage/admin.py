from django.contrib import admin

# Register your models here.
from .models import Exchange
from .models import Bids
from .models import Asks
from .models import Book

admin.site.register(Exchange)
admin.site.register(Bids)
admin.site.register(Asks)
admin.site.register(Book)
