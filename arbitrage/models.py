import logging

from django.core.exceptions import ObjectDoesNotExist
from django.db import models


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class Exchange(models.Model):
    """
    Presents an Exchange market i.e. Kraken, Bitfinex, HitBTC. Each Exchange
    has one or many (order) Books. One can add order book to the Exchange via
    subscribe_book interface.
    """
    name = models.CharField(max_length=100)

    def subscribe_book(self, subscription):
        """
        Creates an unique order book.
        :param subscription: Order book name.
        :return: (Order) Book.
        """
        try:
            book_db = self.book_set.all().get(name=subscription)
        except ObjectDoesNotExist:
            book = self.book_set.create(name=subscription)
            self.save()
            log.debug("'"+subscription+"' initial book successfully created")
        else:
            log.debug("'"+subscription+"' book already exists")
            book = book_db

        return book


class Book(models.Model):
    """
    Present an Exchange order Book. Each Book can have one Exchange and one or
    many bids and asks. To create or update bids/asks use update_bids and
    update_asks interface.
    """
    name = models.CharField(max_length=100)
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)

    def update_bids(self, bids):
        """
        Creates or updates order Book bids.
        :param bids: New/Updated bids.
        """
        bids_db = self.bids_set.all()
        if not bids_db.exists():
            for bid in bids:
                self.bids_set.create(price=bid.price,
                                     count=bid.count,
                                     amount=bid.amount,
                                     timestamp=bid.timestamp)
            self.save()
            log.debug("'"+self.name+"' initial bids successfully created")
        else:
            for index, bid in reversed(list(enumerate(bids))):
                bid_db = bids_db[index]
                bid_db.price = bid.price
                bid_db.count = bid.count
                bid_db.amount = bid.amount
                bid_db.timestamp = bid.timestamp

                bid_db.save(force_update=True)

            log.debug("'"+self.name+"' bids successfully updated!")

    def update_asks(self, asks):
        """
        Creates or updates order Book asks.
        :param asks: New/Updated asks.
        """
        asks_db = self.asks_set.all()
        if not asks_db.exists():
            for ask in asks:
                self.asks_set.create(price=ask.price,
                                     count=ask.count,
                                     amount=ask.amount,
                                     timestamp=ask.timestamp)
            self.save()
            log.debug("'"+self.name+"' initial asks successfully created")
        else:
            for index, ask in reversed(list(enumerate(asks))):
                bid_db = asks_db[index]
                bid_db.price = ask.price
                bid_db.count = ask.count
                bid_db.amount = ask.amount
                bid_db.timestamp = ask.timestamp

                bid_db.save(force_update=True)

            log.debug("'"+self.name+"' asks successfully updated!")


class BookData(models.Model):
    """
    Abstract class presenting order Book asks and bids.
    """
    price = models.DecimalField(decimal_places=10, max_digits=15)
    count = models.IntegerField()
    amount = models.IntegerField()
    timestamp = models.FloatField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Bids(BookData):
    """ Order Book bids. """
    pass


class Asks(BookData):
    """ Order Book asks. """
    pass
