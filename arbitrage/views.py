from django.http import HttpResponse

from arbitrage.bitfinex_manager import BitfinexManager
from arbitrage.kraken_manager import KrakenManager
from arbitrage.hitbtc_manager import HitBTCManager


def index(request):

    subscriptions = ['ETHBTC', 'ETHUSD', 'BTCUSD', 'XRPUSD', 'XRPBTC', 'BCHUSD',
                     'BCHBTC', 'BCHETH']

    bitfinex_manager = BitfinexManager(subscriptions)
    bitfinex_manager.start(3)

    #hitbtcmanager_manager = HitBTCManager(['XRPBTC'])
    #hitbtcmanager_manager.start(7)

    kraken_manager = KrakenManager(['XXBTZUSD', 'XETHZUSD'])
    kraken_manager.start(None)

    return HttpResponse("Searching for arbitrage opportunity ...")
