import logging
import sys
import time


class IManager(object):
    """
    Abstract Exchange (order book) manager class.
    Debug output is by default stored in files.
    """
    def __init__(self, subscriptions):
        """
        Creates IManager with given order book subscription(s).
        :param subscriptions String or List of symbol names for subscription.
        :raise TypeError If empty/wrong type for subscription is passed.
        """
        if isinstance(subscriptions, list):
            self._subscriptions = subscriptions
        elif isinstance(subscriptions, str):
            self._subscriptions = [subscriptions]
        else:
            raise TypeError

        log_format = '%(asctime)s - %(name)-30s - %(levelname)-8s - %(message)s'
        formatter = logging.Formatter(log_format)

        fh = logging.FileHandler(self._log_name()+'.log')
        fh.setFormatter(formatter)
        fh.setLevel(logging.DEBUG)

        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        sh.setLevel(logging.DEBUG)

        self._log = logging.getLogger(self._log_name())
        self._log.addHandler(sh)
        self._log.addHandler(fh)
        self._log.setLevel(logging.INFO)

        self._wss = None
        self._books = []

    def timestamp(self):
        """:return Current time in milliseconds. """
        return int(round(time.time() * 1000))

    def subscribe(self, symbol, timeout):
        """
        Subscribes to given currency pair. Additionally connects to exchange
        API respecting connection timeout parameter.
        :param symbol: Currency pair
        :param timeout: Connection timeout.
        """
        if symbol not in self._subscriptions:
            self._subscriptions.append(symbol)

            book = self._create_book(symbol)
            self._books.append(book)
            if not self.is_connected():
                self._connect(False, timeout)

            if not self.is_connected():
                self._log.error("Unable to connect to '%s'", self.name())
            else:
                self._subscribe_book(book)
                book.start()

    def name(self):
        """
        :returns: Exchange market name.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def _log_name(self):
        """
        :returns: Exchange log name prefix.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def _create_book(self, symbol):
        """
        :param symbol: Order book currency pair name.
        :return: Order book.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def _subscribe_book(self, book):
        """
        Subscribe to order book or list of order books.
        :param book: Order book or list of order books for subscription.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def _connect(self, subscribe, timeout):
        """
        Connects to Exchange market respecting given timeout parameter and
        subscribes to configured order book(s).
        :param subscribe: Subscribes to configured order books if True.
        :param timeout: Connection timeout.
        """
        if self.is_connected() and subscribe:
            self._subscribe_book(self._books)
        else:
            self._log.info("Connecting to '%s' ...", self.name())

            if self._wss is None:
                raise TypeError

            timeout = timeout * 1000
            self._wss.start()

            t0 = self.timestamp()
            while (self.timestamp() - t0) < timeout and not self.is_connected():
                time.sleep(0.25)

            if not self.is_connected():
                self._log.error("Unable to connect to '%s'", self.name())
                self.stop()
            else:
                if subscribe:
                    self._subscribe_book(self._books)

    def _disconnect(self):
        """
        Disconnects Web socket connection.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def is_connected(self):
        """
        :returns: Web socket connection status.
        :raises: NotImplementedError
        """
        raise NotImplementedError

    def start(self, timeout):
        """
        Starts processing of exchange web socket data. Connects to exchange
        market respecting connecting timeout parameter.
        :param timeout: Connection timeout.
        """
        self._log.info("Starting '%s' manager ...", self.name())

        if not self.is_connected():
            self._connect(True, timeout)

        # start queue processing
        if self.is_connected():
            for sub in self._books:
                sub.start()
        else:
            self._log.error("An error occurred while trying to connect to "
                            + "'%s' exchange. Unable to start '%s' manager.",
                            self.name(), self.name())

    def stop(self):
        """ Disconnects and stops exchange market processing and  """
        self._log.info("Stopping '%s' manager ...", self.name())

        if self.is_connected():
            for sub in self._books:
                sub.stop()
            self._disconnect()
