from btfxwss import BtfxWss

from arbitrage.bitfinex_book import BitfinexBook
from arbitrage.imanager import IManager


class BitfinexManager(IManager):
    """
    Bitfinex Exchange (order book) manager class.
    """
    def __init__(self, subscriptions):
        """
        Creates BitfinexManager with given order book subscription(s).
        :param subscriptions: String or list of order book subscriptions.
        """
        super(BitfinexManager, self).__init__(subscriptions)

        self._wss = BtfxWss()

        for subscription in self._subscriptions:
            self._books.append(BitfinexBook(self._wss, subscription))

    def _create_book(self, symbol):
        """ :returns: BitfinexBook with given currency pair. """
        return BitfinexBook(self._wss, symbol)

    def _subscribe_book(self, book):
        """" Subscribes to given order book name(s). """
        if isinstance(book, list):
            for book_token in book:
                self._log.info("Subscribing to book '%s'", book_token.symbol)
                self._wss.subscribe_to_order_book(book_token.symbol)
        else:
            self._log.info("Subscribing to book '%s'", book.symbol)
            self._wss.subscribe_to_order_book(book.symbol)

    def _disconnect(self):
        """ Stops web socket stream. """
        self._wss.stop()

    def is_connected(self):
        """ :returns: True if web socket is connected; else False."""
        return self._wss.conn.connected.is_set()

    def name(self):
        """ :returns: Bitfinex exchange name."""
        return "Bitfinex"

    def _log_name(self):
        """" :returns: BitfinexManager log name. """
        return __name__
