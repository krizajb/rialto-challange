import logging
import queue

from arbitrage.ibook import IBookHandler, BookData
from threading import Event, Thread


class KrakenBook(Thread, IBookHandler):
    """
    Kraken order book implementation.
    """
    def __init__(self, book, *args, **kwargs):
        """
        Creates KrakenBook with order book (subscription) name.
        To start web socket message processing use start() interface. Each order
        book update triggers arbitrage opportunity calculation.

        :param book: Book (subscription) name.
        :param args: Optional args passed to Thread.
        :param kwargs: Optional kwargs passed to Thread.
        """
        self.symbol_mappings = {'XXBTZUSD': 'BTCUSD',
                                'XETHZUSD': 'ETHUSD'}

        IBookHandler.__init__(self, None, __name__, book, *args, **kwargs)
        Thread.__init__(self, *args, **kwargs)

        self._stopped = Event()
        self.limit = 20

        self.set_log_level(logging.DEBUG)

        self.queue = queue.Queue(100)

    def run(self):
        """ Main routine. Processing web socket order book data. """
        while not self._stopped.is_set():
            try:
                data = self.queue.get()
                self._handle_data(data)
            except queue.Empty:
                continue

    def stop(self, timeout=None):
        """Set sentinel for run() method and join thread.
        :param timeout: Thread.join timeout
        """
        self._stopped.set()
        super(KrakenBook, self).join(timeout=timeout)

    def symbol_mapping(self):
        """ :returns: Bitfinex currency pair mapped to configured order book."""
        return self.symbol_mappings[self.symbol]

    def _handle_data(self, data):
        """
        Data handler for Kraken exchange order book.
        :param data: Web socket data.
        """
        error = data['error']
        if error:
            self._log.error(error)
            return

        result = data['result'][self.symbol]

        asks = result['asks']
        bids = result['bids']

        self._book.asks = {}
        self._book.bids = {}

        for ask in asks:
            book_data = BookData(float(ask[0]), ask[1], None, ask[2])
            self._book.asks.update({book_data.price: book_data})

        for bid in bids:
            book_data = BookData(float(bid[0]), bid[1], None, bid[2])
            self._book.bids.update({book_data.price: book_data})

        self.arbitrage_opportunity(1)
