import logging
import time
import queue
from threading import Event, Thread

from arbitrage.ibook import IBookHandler
from arbitrage.ibook import BookData
from arbitrage.models import Book


class HitBTCBook(Thread, IBookHandler):
    """
    HitBTC order book implementation.
    """
    def __init__(self, socket, book, *args, **kwargs):
        """
        Creates HitBTCBook with order book (subscription) name and optional
        web socket reference. To start web socket message processing use start()
        interface. Each order book update triggers arbitrage opportunity
        calculation.

        :param socket: HitBTC Web socket API reference.
        :param book: Book (subscription) name.
        :param args: Optional args passed to Thread.
        :param kwargs: Optional kwargs passed to Thread.
        """
        self.symbol_mappings = {}

        IBookHandler.__init__(self, socket, __name__, book, *args, **kwargs)
        Thread.__init__(self, *args, **kwargs)

        self._stopped = Event()
        self._limit = 20

        self.set_log_level(logging.INFO)

        # handling only order book responses for the sake of simplicity
        self._data_handlers = {'snapshotOrderbook': self._handle_init_book,
                               'updateOrderbook': self._handle_book}

        self._bitfinex_book_db = None
        self._database_book()

        self.queue = queue.Queue(100)

    def _database_book(self):
        """ :returns: BitfinexBook database mapped to the same order book
        currency pair. """
        if self._bitfinex_book_db is None:
            if Book.objects.filter(name=self.symbol).exists():
                self._bitfinex_book_db = Book.objects.get(name=self.symbol)

    def symbol_mapping(self):
        """ :returns: Bitfinex currency pair mapped to configured order book."""
        return self.symbol_mappings[self.symbol]

    def run(self):
        """
        Main routine. Processing web socket order book data. Additionally
        establishes web socket connection with HitBTC and subscribes to the
        configured order book(s).
        """
        if not self._wss.conn._is_connected:
            self._wss.start()

            # todo: introduce timeout routine
            while not self._wss.conn._is_connected:
                time.sleep(0.25)

            self._wss.subscribe_book(symbol=self.symbol,
                                     limit=self._limit)

        while not self._stopped.is_set():
            try:
                data = self.queue.get()
                self._handle_data(data)
            except queue.Empty:
                continue

    def stop(self, timeout=None):
        """Set sentinel for run() method and join thread.
        :param timeout: Thread.join timeout
        """
        self._log.info("Unsubscribing '%s'", self.symbol)

        self._wss.subscribe_book(symbol=self.symbol, cancel=True)
        self._wss.stop()

        self._stopped.set()
        super(HitBTCBook, self).join(timeout=timeout)

    def _handle_data(self, message):
        """
        Data handler for HitBTC exchange order book.
        :param message: Web socket data.
        """
        method, symbol, params = message

        # response handling already implemented by HitBTCConnector
        # one could attach addition handler for errors by set requirement
        # to catch error check "symbol" for value 'Failure'
        if method == 'Response':
            return

        if method in ('snapshotOrderbook', 'updateOrderbook'):
            try:
                self._data_handlers[method](params)
            except KeyError:
                self._log.error("Unsupported data method '%s' handler")
        else:
            self._log.info("Unsupported method '%s'")

    def _handle_init_book(self, data):
        """
        Handles order book initial snapshoot data.
        :param data: Snapshoot data.
        """
        # handle order book snapshot and create in-memory book structure
        asks = data['ask']
        bids = data['bid']

        # note: limit of order book levels (default 100)
        # couldn't get subscription parameter 'limit' to work
        # and it seems as if a lot of the snapshot data is stale
        count = 0
        ask_loop = (x for x in asks if count < self._limit)
        for ask in ask_loop:
            book_data = BookData(float(ask['price']), float(ask['size']), None)
            self._book.asks.update({book_data.price: book_data})
            count += 1

        count = 0
        bid_loop = (x for x in bids if count < self._limit)
        for bid in bid_loop:
            book_data = BookData(float(bid['price']), float(bid['size']), None)
            self._book.bids.update({book_data.price: book_data})
            count += 1

        self._init = False

    def _handle_book(self, data):
        """
        Handles order book update data.
        :param data: Update data.
        """
        # handle order book updates
        asks = data['ask']
        bids = data['bid']

        for ask in asks:
            size = float(ask['size'])
            price = float(ask['price'])
            if 0 == size:
                try:
                    del self._book.asks[price]
                except KeyError:
                    self._log.debug("Unable to delete book, side '%s' not "
                                    "found", price)
            else:
                book_data = BookData(price, size, None)
                self._book.asks.update({book_data.price: book_data})

        for bid in bids:
            size = float(bid['size'])
            price = float(bid['price'])
            if 0 == size:
                try:
                    del self._book.bids[price]
                except KeyError:
                    self._log.debug("Unable to delete book, side '%s' not "
                                    "found", price)
            else:
                book_data = BookData(price, size, None)
                self._book.bids.update({book_data.price: book_data})

        # check for arbitrage opportunity
        self.arbitrage_opportunity(1)
