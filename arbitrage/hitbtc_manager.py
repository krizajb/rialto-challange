import queue
from threading import Event

from hitbtc import HitBTC

from arbitrage.hitbtc_book import HitBTCBook
from arbitrage.imanager import IManager


class HitBTCManager(IManager):
    """
    HitBTCManager Exchange (order book) manager class.
    """
    def __init__(self, subscriptions):
        """
        Creates HitBTCManager with given order book subscription(s).
        :param subscriptions: String or list of order book subscriptions.
        """
        super(HitBTCManager, self).__init__(subscriptions)

        self._stopped = Event()

        self._wss = HitBTC()
        self._limit = 100

        # same as _books but dict type
        self._books_dict = {}

        for subscription in self._subscriptions:
            book = self._create_book(subscription)
            self._books.append(book)
            self._books_dict.update({subscription: book})

    def name(self):
        """ :returns: HitBTC exchange name."""
        return "HitBTC"

    def _log_name(self):
        """" :returns: HitBTCManager log name. """
        return __name__

    def _create_book(self, book_name):
        """ :returns: HitBTCBook with given currency pair. """
        return HitBTCBook(self._wss, book_name)

    def _subscribe_book(self, book):
        """" Subscribes to given order book name(s). """
        if isinstance(book, list):
            for book_token in book:
                self._log.info("Subscribing to book '%s'", book_token.symbol)
                self._wss.subscribe_book(symbol=book_token.symbol,
                                         limit=self._limit)
        else:
            self._log.info("Subscribing to book '%s'", book.symbol)
            self._wss.subscribe_book(symbol=book.symbol, limit=self._limit)

    def start(self, timeout):
        """
        Dispatches data to appropriate order book. Connects to exchange
        market respecting connecting timeout parameter.
        :param timeout: Connection timeout.
        """
        self._log.info("Starting '%s' manager ...", self.name())

        # connect and subscribe
        if not self.is_connected():
            self._connect(True, timeout)

        # start order book queue processing
        if self.is_connected():
            for sub in self._books:
                sub.start()

        # dispatch wss data to order book
        while not self._stopped.is_set():
            try:
                data = self._wss.recv()
                method, symbol, params = data

                # response handling already implemented by HitBTCConnector
                # one could attach addition handler for errors by a set of
                # requirements
                #  to catch error check "symbol" for value 'Failure'
                if method == 'Response':
                    continue

                self._books_dict[symbol].queue.put(data)
            except queue.Empty:
                continue

    def _disconnect(self):
        """ Stops web socket stream. """
        self._wss.stop()

    def is_connected(self):
        """ :returns: True if web socket is connected; else False."""
        # note: accessing protected member, see issue #27
        return self._wss.conn._is_connected