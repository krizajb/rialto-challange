from threading import Event

import krakenex

from requests.exceptions import HTTPError

from arbitrage.imanager import IManager
from arbitrage.kraken_book import KrakenBook


class KrakenManager(IManager):
    """
    KrakenManager Exchange (order book) manager class.
    """
    def __init__(self, subscriptions):
        """
        Creates KrakenManager with given order book subscription(s).
        :param subscriptions: String or list of order book subscriptions.
        """
        super(KrakenManager, self).__init__(subscriptions)

        self._stopped = Event()

        self._wss = krakenex.API()

        for subscription in self._subscriptions:
            book = self._create_book(subscription)
            self._books.append(book)

    def name(self):
        """ :returns: Kraken exchange name."""
        return "Kraken"

    def _log_name(self):
        """" :returns: KrakenManager log name. """
        return __name__

    def _create_book(self, book_name):
        """ :returns: KrakenBook with given currency pair. """
        return KrakenBook(book_name)

    def _subscribe_book(self, book):
        """ NOP """
        pass

    def start(self, timeout):
        """
        Dispatches data to appropriate order book. Starts order book processing.
        :param timeout: Unused.
        """
        self._log.info("Starting '%s' manager ...", self.name())

        # start order book queue processing
        for book in self._books:
            book.start()

        while not self._stopped.is_set():
            try:
                for book in self._books:
                    request = {'pair': book.symbol, 'count': book.limit}
                    response = self._wss.query_public('Depth', request)
                    book.queue.put(response)
            except HTTPError as e:
                self._log.error(str(e))

    def _disconnect(self):
        """ NOP """
        pass

    def is_connected(self):
        """ NOP """
        return True
