import logging
import sys

from arbitrage.models import Book as BookModel


class BookData(object):
    """
    Book (bid/ask) entry consisting of price, count, amount and timestamp.
    """
    __slots__ = ["price", "count", "amount", "timestamp"]

    def __init__(self, price, count, amount, timestamp=None):
        """
        Creates order book bid/ask entry.
        :param price: Price.
        :param count: Count.
        :param amount: Amount.
        :param timestamp: Timestamp.
        """
        self.price = price
        self.count = count
        self.amount = amount
        self.timestamp = timestamp

    def __str__(self):
        """ :returns Book bid/ask as string. """
        return str(self.price)+" "+str(self.count)+" " \
               + str(self.amount)+" "+str(self.timestamp)


class Book(object):
    """
    Order book consisting of bids and asks.
    """
    def __init__(self):
        """
        Creates order Book.
        """
        self.bids = {}
        self.asks = {}


class IBookHandler(object):
    """
    Abstract BookHandler class offering arbitrage opportunity calculation if
    order book mapping to the appropriate currency pair is provided via
    symbol_mapping interface.

    Subclasses can access book data via _book attribute. In case order book
    data consists of initial snapshoot one can use _init flag.

    Debug output is by default stored in files.
    """
    def __init__(self, socket, exchange, book, *args, **kwargs):
        """
        Creates IBookHandler with given web socket reference, exchange market
        name and order book currency pair.
        :param socket: Exchange web socket reference.
        :param exchange: Exchange market name.
        :param book: Order book currency pair.
        :param args: Optional args.
        :param kwargs: Optional kwargs.
        :raises TypeError and ValueError for order book currency pair.
        """
        if not isinstance(book, str):
            raise TypeError
        elif not book:
            raise ValueError
        else:
            self.symbol = book

        self._wss = socket

        log_format = '%(asctime)s - %(name)-25s - %(levelname)-8s - %(message)s'
        formatter = logging.Formatter(log_format)

        fh = logging.FileHandler(exchange.lower()+"_"+book.lower()+'.log')
        fh.setFormatter(formatter)
        fh.setLevel(logging.DEBUG)

        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        sh.setLevel(logging.DEBUG)

        self._log = logging.getLogger(exchange.lower()+"."+book.lower())
        self._log.addHandler(sh)
        self._log.addHandler(fh)
        self._log.setLevel(logging.INFO)

        self._book = Book()

        self._bitfinex_book_db = None
        self._database_book()

        self._init = True

    def _database_book(self):
        """
        :return: Bitfinex order book database for configured currency pair.
        """
        if self._bitfinex_book_db is None:
            symbol = self.symbol_mapping()
            if BookModel.objects.filter(name=symbol).exists():
                self._bitfinex_book_db = BookModel.objects.get(name=symbol)

    def symbol_mapping(self):
        """ Used for database lookup.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def set_log_level(self, level):
        """ Sets log level. """
        self._log.setLevel(level)

    def _handle_data(self, data):
        """ Data handler.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def stop(self, timeout=None):
        """Set sentinel for run() method and join thread.
        :param timeout: Thread.join timeout
        :raises NotImplementedError
        """
        raise NotImplementedError

    def run(self):
        """
        Main routine.
        :raises NotImplementedError
        """
        raise NotImplementedError

    def arbitrage_opportunity(self, spread_percent):
        """
        Calculates arbitrage opportunity with given spread percent. Correct
        database/currency sign mapping must be provided.
        :param spread_percent: 1% spread.
        """
        if self._bitfinex_book_db is None:
            self._database_book()
            return

        for this_bid in self._book.bids.keys():
            for ask in self._bitfinex_book_db.asks_set.all():
                bitfinext_ask = float(ask.price)
                difference = this_bid/bitfinext_ask
                self._log.debug("this bid %f / bitfinex ask %f rate '%f'",
                                this_bid, bitfinext_ask, difference)
                if difference > 1:
                    difference_percent = (difference - 1) * 100
                    if difference_percent > spread_percent:
                        self._log.info("ARB. OPPORTUNITY   '%f'", difference)
                        self._log.info("this bid price     '%f'", this_bid)
                        self._log.info("biftinex ask price '%f'", bitfinext_ask)
                        self._log.info("------------------------------")

        for this_ask in self._book.asks.keys():
            for bid in self._bitfinex_book_db.bids_set.all():
                bitfinext_bid = float(bid.price)
                difference = bitfinext_bid/this_ask
                self._log.debug("this ask %f / bitfinex bid %f rate '%f'",
                                this_ask, bitfinext_bid, difference)
                if difference < 1:
                    difference_percent = (1 - difference) * 100
                    if difference_percent > spread_percent:
                        self._log.info("ARB. OPPORTUNITY 2 '%f'", difference)
                        self._log.info("biftinex bid price '%f'", bitfinext_bid)
                        self._log.info("this ask price     '%f'", this_ask)
                        self._log.info("------------------------------")
