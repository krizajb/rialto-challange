# Rialto arbitrage challenge

Railto arbitrage challenge is a crypto arbitrage opportunity detector 
implemented in Python 3 using Django 2 framework.

### Instructions

##### Clone the vagrant repo
```bash
$ git clone -b rialto https://github.com/krizajb/vagrant-lamp-centos.git
```
Follow this [steps](https://github.com/krizajb/vagrant-lamp-centos/tree/rialto#usage)
to install development environment for the Railto arbitrage challenge.

##### Install vagrant Virtual machine
Running ``` vagrant up ``` will install naked centOS 7 with all required packages
to run Rialto arbitrage project and clone this repo into VM synced folder.

##### Run Django server
```
$ vagrant ssh
$ cd /opt/webroot/rialtochallange
$ sudo python3 manage.py runserver 10.10.0.20:8000

Open http://10.10.0.20:8000/ in a browser.
```

##### Access database
```
Open http://10.10.0.20:8000/admin in a browser.
username: admin
password: rialtoadmin
```

#### APIs used
- [hitbtc](https://github.com/Crypto-toolbox/hitbtc) 
- [btfxwss](https://github.com/Crypto-toolbox/btfxwss) 
- [krakenex](https://github.com/veox/python3-krakenex) 

#### References
- [Kraken API documentation](https://www.kraken.com/help/api)
- [Bitfinex API documentation](https://bitfinex.readme.io/v2/docs)
- [HitBTC API documentation](https://api.hitbtc.com/)